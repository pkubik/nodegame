/**
 * 
 */
package pw.eiti.pkubik.proz.game;

import javafx.application.Application;
import javafx.stage.Stage;
import pw.eiti.pkubik.proz.game.controller.Controller;
import pw.eiti.pkubik.proz.game.view.View;

/**
 * Main program class
 */
public class Main extends Application
{
    private Controller controller;
    private View view;
    
    /**
     * Fallback main method
     * @param args command line parameters
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    /**
     * JavaFX start point
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override
    public void start(Stage stage)
    {
        controller = new Controller();
        view = new View(stage, controller);
        controller.setView(view);
        controller.start();
    }
    
    @Override
    public void stop()
    {
        controller.exit();
    }
}

// just CI test