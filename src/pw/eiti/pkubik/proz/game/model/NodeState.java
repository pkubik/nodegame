package pw.eiti.pkubik.proz.game.model;

import java.util.UUID;

public class NodeState
{
    private final Position position;
    private final UUID ownerId;
    private final int formationSize;
    private final double formationAttack;
    private final double formationSpeed;
    private final double productionRate;
   
    public NodeState(Node node)
    {
        position = node.getPosition();
        ownerId = node.getOwner().getId();
        formationSize = node.getDefendingFormation().getSize();
        formationAttack = node.getDefendingFormation().getAttributes().getAttack();
        formationSpeed = node.getDefendingFormation().getAttributes().getSpeed();
        productionRate = node.getFactory().getProductionRate();
    }

    public Position getPosition()
    {
        return position;
    }

    public UUID getOwnerId()
    {
        return ownerId;
    }

    public int getFormationSize()
    {
        return formationSize;
    }

    public double getFormationAttack()
    {
        return formationAttack;
    }

    public double getFormationSpeed()
    {
        return formationSpeed;
    }

    public double getProductionRate()
    {
        return productionRate;
    }
}
