package pw.eiti.pkubik.proz.game.model;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * An event to occur after given delay
 */
abstract class DelayedEvent implements Delayed
{
    /** manages in-game time */
    private final Clock clock;
    /** In-game time when event will occur */
    private long startTime;
    
    /**
     * Determines which event should occur first
     * @param another Event to be compared
     * @return Positive if this should occur first, negative if another, and 0 otherwise
     */
    private int compareToEvent(DelayedEvent another)
    {
        return (int) java.lang.Math.signum(startTime - another.startTime);
    }
    
    /**
     * Create immediate event
     * @param clock Clock holding in-game time
     */
    DelayedEvent(final Clock clock)
    {
        this.clock = clock;
        this.startTime = clock.nanoTime();
    }
    
    /**
     * Create delayed event
     * @param clock Clock holding in-game time
     * @param delay Time after which event occur
     */
    DelayedEvent(final Clock clock, final long delay)
    {
        this.clock = clock;
        this.startTime = clock.nanoTime() + delay;
    }
    
    /*
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Delayed another)
    {
        return compareToEvent((DelayedEvent) another);
    }
    
    /*
     * @see java.util.concurrent.Delayed#getDelay(java.util.concurrent.TimeUnit)
     */
    @Override
    public long getDelay(TimeUnit unit)
    {
        long timeDifference = startTime - clock.nanoTime();
        return unit.convert(timeDifference, TimeUnit.NANOSECONDS);
    }
    
    /**
     * Function defining the event
     */
    abstract void occur();
}
