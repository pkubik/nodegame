package pw.eiti.pkubik.proz.game.model;

import java.util.UUID;

public class TransferState
{
    private final UUID id;
    private final Position source;
    private final Position target;
    private final long startTime;
    private final long overallTime;
    
    public TransferState(final Transfer transfer)
    {
        id = transfer.getId();
        source = transfer.getSource().getPosition();
        target = transfer.getDestination().getPosition();
        startTime = transfer.getStartTime();
        overallTime = transfer.getOverallTime();
    }

    public UUID getId()
    {
        return id;
    }
    public Position getSource()
    {
        return source;
    }

    public Position getTarget()
    {
        return target;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public long getOverallTime()
    {
        return overallTime;
    }
    
    public double getProgress(final long time)
    {
        return (double) (time - startTime) / overallTime;
    }
}