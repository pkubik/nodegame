package pw.eiti.pkubik.proz.game.model;

import java.util.concurrent.DelayQueue;

/**
 * Periodically adds new units to the formation on the same node.
 * It isn't related to the factory design pattern.
 */
class Factory
{
    /** Nanoseconds between each production event assuming that production rate equals 1.0 */
    private static final long BASE_PRODUCTION_DELAY = 1000000000;
    
    /** Game instance root */
    private final Game game;
    /** Output node */
    private final Node node;
    /** Attributes of produced units */
    private FleetAttributes attributes;
    /** Production rate */
    private final double productionRate;
    /** Reference to main events queue */
    private DelayQueue<DelayedEvent> queue;
    
    /**
     * @param clock manages in-game time
     * @param node output node
     * @param attributes Attributes of produced units
     * @param productionRate production rate
     */
    public Factory(final Game game, final Node node, final FleetAttributes attributes, final double productionRate)
    {
        this.game = game;
        this.node = node;
        this.attributes = attributes;
        this.productionRate = productionRate;
    }
    
    /**
     * Gets the output formation.
     * @return the output formation
     */
    public Formation getOutputFormation()
    {
        return node.getDefendingFormation();
    }
    
    /**
     * @return Attributes of produced units
     */
    public FleetAttributes getAttributes()
    {
        return attributes;
    }
    
    public double getProductionRate()
    {
        return productionRate;
    }
    
    /**
     * Produce unit and put it into output formation
     */
    public void produce()
    {
        getOutputFormation().merge(attributes, 1);
    }
    
    /**
     * Initialize or renew production process
     * @return either new or delayed production event
     */
    public void initiateProduction(final DelayQueue<DelayedEvent> queue)
    {
        if (this.queue == null)
        {
            this.queue = queue;
            this.queue.add(new ProductionEvent(getProductionTime()));                        
        }
    }
    
    public long getProductionTime()
    {
        return (long) (BASE_PRODUCTION_DELAY / productionRate);
    }
    
    /**
     * Repetitively produces new unit
     */
    class ProductionEvent extends DelayedEvent
    {   
        public ProductionEvent(final long delay)
        {
            super(game.getClock(), delay);
        }
        
        /**
         * Produces unit, and put new production event to the queue
         * @see pw.eiti.pkubik.proz.game.model.DelayedEvent#occur(java.util.concurrent.DelayQueue)
         */
        @Override
        public void occur()
        {
            produce();
            queue.add(new ProductionEvent(getProductionTime()));
        }
    }
}
