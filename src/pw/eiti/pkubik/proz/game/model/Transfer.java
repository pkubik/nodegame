package pw.eiti.pkubik.proz.game.model;

import java.util.Collection;
import java.util.UUID;

class Transfer
{
    /** Game instance root */
    private final Game game;
    private final UUID id = UUID.randomUUID();
    private final Formation formation;
    private final Node source;
    private final Node destination;
    private final long overallTime;
    private long startTime;
    
    public Transfer(final Game game, final Formation formation, final Node source, final Node destination, final double distance)
    {
        this.game = game;
        this.formation = formation;
        this.source = source;
        this.destination = destination;
        overallTime = (long) (distance*5000000000L / formation.getAttributes().getSpeed());
    }

    public UUID getId()
    {
        return id;
    }
    
    public Formation getFormation()
    {
        return formation;
    }

    public Node getSource()
    {
        return source;
    }

    public Node getDestination()
    {
        return destination;
    }

    public long getOverallTime()
    {
        return overallTime;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public TransferEvent getTransferEvent(final Collection<Transfer> transfers)
    {
        startTime = game.getClock().nanoTime();
        return new TransferEvent(overallTime, transfers);
    }
    
    class TransferEvent extends DelayedEvent
    {          
        private final Collection<Transfer> transfers;
        
        public TransferEvent(final long delay, final Collection<Transfer> transfers)
        {
            super(game.getClock(), delay);
            this.transfers = transfers;
            transfers.add(Transfer.this);
        }
        
        @Override
        public void occur()
        {
            destination.receiveUnits(formation);
            transfers.remove(Transfer.this);
        }
    }
}
