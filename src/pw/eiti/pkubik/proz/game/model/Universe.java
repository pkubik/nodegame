package pw.eiti.pkubik.proz.game.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.DelayQueue;

import com.google.gson.Gson;

/**
 * represents game state
 */
class Universe
{
    private static final String CONFIG_FILE = "res/map.json";
    
    /** manages in-game time */
	private final Game game;
    private final Map<Position, Node> nodes = new HashMap<Position, Node>();
    private final List<Transfer> transfers = new LinkedList<Transfer>();
    private final List<Player> players = new ArrayList<Player>();
    
    /** Manages events according to in-game time */
    private final DelayQueue<DelayedEvent> events = new DelayQueue<DelayedEvent>();
    
    private void loadFromFile(final String path) throws IOException
    {
        Reader reader = new FileReader(new File(path));
        Gson gson = new Gson();
        GameTemplate template = gson.fromJson(reader, GameTemplate.class);
        
        final int activePlayerIndex = template.getActivePlayer();
        int playerIndexCounter = 0; 
        
        for (GameTemplate.Player templatePlayer : template.getPlayers())
        {
            Player player = new Player(templatePlayer.getName());
            players.add(player);
            for (GameTemplate.Planet planet : templatePlayer.getPlanets())
            {
                nodes.put(new Position(planet.getX(), planet.getY()),
                          new Node(game, planet.getX(), planet.getY(), player, 
                                  new FleetAttributes(planet.getAttack(), planet.getSpeed()),
                                  planet.getProductionRate()));
            }
            if (playerIndexCounter == activePlayerIndex)
            {
                game.setActivePlayer(player);
            }
            ++playerIndexCounter;
        }
        
        players.add(game.getNeutralPlayer());
        for (GameTemplate.Planet planet : template.getFreePlanets())
        {
            nodes.put(new Position(planet.getX(), planet.getY()),
                      new Node(game, planet.getX(), planet.getY(), game.getNeutralPlayer(),
                              new FleetAttributes(planet.getAttack(), planet.getSpeed()),
                              planet.getProductionRate()));
        }
        reader.close();
    }
    
    public Universe(final Game game) throws IOException
    {
        this.game = game;        
        loadFromFile(CONFIG_FILE);
        initiateNodesProduction();
    }
    
    public Map<Position, Node> getNodes()
    {
        return nodes;
    }

    public List<Transfer> getTransfers()
    {
        return transfers;
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    /**
     * Adds repeating production event of each node's factory
     */
    private void initiateNodesProduction()
    {
        for (Node node : nodes.values())
        {
            node.getFactory().initiateProduction(events);
        }
    }   

    /**
     * Advance the game by given time
     * @param delay in-game time elapsed
     */
    public void update(final long delay)
    {
        game.getClock().delay(delay);
        DelayedEvent event;

        event = events.poll();
        while (event != null)
        {
            event.occur();
            event = events.poll();
        }
    }
    
    public void transferUnits(final Node source, final Node destination)
    {
        if (game.getActivePlayer().equals(source.getOwner()))
        {
            Transfer transfer = source.transferUnits(destination);
            events.add(transfer.getTransferEvent(transfers));
        }
    }
}
