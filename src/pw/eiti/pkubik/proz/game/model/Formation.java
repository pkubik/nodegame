package pw.eiti.pkubik.proz.game.model;

/**
 * Represents a group units
 */
class Formation
{
    /** Game instance root */
    private final Game game;
    private final Player owner;
    /** number of units */
    private int size;
    private FleetAttributes attributes;
    
    /**
     * @param owner player controlling this Formation
     * @param size initial size of this Formation
     * @param attributes initial {@link #FleetAttributes} of this Formation
     */
    public Formation(final Game game, final Player owner, final int size, final FleetAttributes attributes)
    {
        this.game = game;
        this.owner = owner;
        this.size = size;
        this.attributes = attributes;
    }
    
    /**
     * @return player controlling this Formation
     */
    public Player getOwner()
    {
        return owner;
    }

    /**
     * @return number of units in this Formation
     */
    public int getSize()
    {
        return size;
    }
    
    public FleetAttributes getAttributes()
    {
        return attributes;
    }
    
    /**
     * Merges formation with specified number of units of specified attributes
     * @param attributes
     * @param size number of units
     */
    public void merge(final FleetAttributes attributes, final int size)
    {
        if (owner != game.getNeutralPlayer())
        {
            this.attributes = this.attributes.add(attributes, this.size, size);
            this.size += size;
        }
    }
    
    /**
     * @return half of the formation, return value is subtracted from this
     */
    public Formation extractHalf()
    {
        final int newSize = size / 2;
        final double newAttack = attributes.getAttack() / 2;
        attributes = new FleetAttributes(attributes.getAttack() - newAttack, attributes.getSpeed());
        size -= newSize;
        return new Formation(game, owner, newSize, new FleetAttributes(newAttack, attributes.getSpeed()));
    }
    
    /**
     * An action performed when two formations meet on the same Node
     * Arriving Formation is considered the winner if it was able
     * to completely destroy this Formation. Otherwise this formation
     * is the winner. 
     * @param formation arriving Formation
     * @return winner of the confrontation
     */
    public Formation confront(final Formation formation)
    {
        if (owner.equals(formation.owner))
        {
            merge(formation.attributes, formation.size);
            return this;
        }
        else
        {
            Formation winner;
            Double attackDifference = attributes.getAttack() - formation.attributes.getAttack();
            if (attackDifference == 0)
            {
                return new Formation(game, game.getNeutralPlayer(), 0, attributes);
            }
            else if (attackDifference > 0)
            {
                winner = this;
            }
            else
            {
                attackDifference = -attackDifference;
                winner = formation;
            }
            
            winner.size = (int) ( attackDifference / winner.attributes.getAttack() * winner.size );
            winner.attributes = new FleetAttributes(attackDifference, winner.attributes.getSpeed());
            return winner;
        }
    }
}
