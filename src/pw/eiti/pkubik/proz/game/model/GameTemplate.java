package pw.eiti.pkubik.proz.game.model;

import java.util.List;

/**
 * Template used to read JSON file
 */
class GameTemplate
{
    private List<Player> players;
    /** planets not belonging to any player */
    private List<Planet> freePlanets;
    /** An index of player that is to be set active */
    private int activePlayer;
    
    /**
     * Planet is equivalent of Node
     */
    public static class Planet
    {
        /** Position coordinate */
        private Double x;
        /** Position coordinate */
        private Double y;
        /** Attack strength of produced units */
        private Double attack;
        /** Speed of produced units */
        private Double speed;
        /** Production rate */
        private Double productionRate;
        
        /**
         * @return X coordinate
         */
        public Double getX()
        {
            return x;
        }
        /**
         * @return Y coordinate
         */
        public Double getY()
        {
            return y;
        }
        /**
         * Used by parser
         * @param x X coordinate
         */
        public void setX(Double x)
        {
            this.x = x;
        }
        /**
         * Used by parser
         * @param x Y coordinate
         */
        public void setY(Double y)
        {
            this.y = y;
        }
        
        /**
         * @return Attack strength of produced units
         */
        public Double getAttack()
        {
            return attack;
        }
        /**
         * @return Speed of produced units
         */
        public Double getSpeed()
        {
            return speed;
        }
        public Double getProductionRate()
        {
            return productionRate;
        }
        public void setAttack(Double attack)
        {
            this.attack = attack;
        }
        public void setSpeed(Double speed)
        {
            this.speed = speed;
        }
        public void setProductionRate(Double productionRate)
        {
            this.productionRate = productionRate;
        }
    }
    
    public static class Player
    {
        /** player name */
        private String name;
        /** planets belonging to this player */
        private List<Planet> planets;
        
        public String getName()
        {
            return name;
        }
        public List<Planet> getPlanets()
        {
            return planets;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public void setPlanets(List<Planet> planets)
        {
            this.planets = planets;
        }
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public void setPlayers(List<Player> players)
    {
        this.players = players;
    }

    public List<Planet> getFreePlanets()
    {
        return freePlanets;
    }

    public void setFreePlanets(List<Planet> freePlanets)
    {
        this.freePlanets = freePlanets;
    }

    public int getActivePlayer()
    {
        return activePlayer;
    }

    public void setActivePlayer(int activePlayer)
    {
        this.activePlayer = activePlayer;
    }
}
