package pw.eiti.pkubik.proz.game.model;

import java.util.ArrayList;
import java.util.List;

public class GameState
{
    /** Current value of game-time */
    private final long time;
    
    private final List<NodeState> nodes = new ArrayList<NodeState>();
    private final List<TransferState> transfers = new ArrayList<TransferState>();
    private final List<PlayerState> players = new ArrayList<PlayerState>();
    
    /** Allows to apply distinction to non-controllable player */
    private final PlayerState nullPlayer;
    
    GameState(final Game game)
    {
        nullPlayer = new PlayerState(game.getNeutralPlayer());
        final Universe universe = game.getUniverse();
        time = game.getClock().nanoTime();
        
        for (Node node : universe.getNodes().values())
        {
            nodes.add(new NodeState(node));
        }
        
        for (Transfer transfer : universe.getTransfers())
        {
            transfers.add(new TransferState(transfer));
        }
        
        for (Player player : universe.getPlayers())
        {
            players.add(new PlayerState(player));
        }
    }

    /**
     * @return current value of game-time in nanoseconds
     */
    public long getTime()
    {
        return time;
    }

    /**
     * @return Nodes states
     */
    public List<NodeState> getNodes()
    {
        return nodes;
    }
    
    /**
     * @return Transfers states
     */
    public List<TransferState> getTransfers()
    {
        return transfers;
    }
    
    /**
     * @return Players states
     */
    public List<PlayerState> getPlayers()
    {
        return players;
    }

    /**
     * @return State of neutral player
     */
    public PlayerState getNullPlayer()
    {
        return nullPlayer;
    }
}
