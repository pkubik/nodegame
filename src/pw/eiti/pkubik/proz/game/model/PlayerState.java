package pw.eiti.pkubik.proz.game.model;

import java.util.UUID;

public class PlayerState
{
    private final UUID id;
    private final String name;
    
    public PlayerState(final Player player)
    {
        id = player.getId();
        name = player.getName();
    }

    public UUID getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
