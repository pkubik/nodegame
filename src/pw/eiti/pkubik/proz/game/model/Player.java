package pw.eiti.pkubik.proz.game.model;

import java.util.UUID;

/**
 * Represents a player. Player may own Formations
 * and what affect it's confrontations resolutions.
 */
class Player
{   
    private final UUID id = UUID.randomUUID();
    private final String name;
    
    public Player(final String name)
    {
        this.name = name;
    }
    
    public UUID getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
