package pw.eiti.pkubik.proz.game.model;

/**
 * Represents node state. Ownership of the node 
 * is determined by ownership of the defending formation
 */
class Node
{
    /** Game instance root */
    private final Game game;
    /** The center of planet */
    private final Position position;
    /** Factory which output units to this node's defending formation */
    private final Factory factory;
    /** Formation stationing on this node */
    private Formation defendingFormation;
    
    /**
     * @param clock manages in-game time
     * @param x X coordinate of planet's center
     * @param y Y coordinate of planet's center
     * @param owner initial owner of the planet
     * @param attributes attributes of ships produced on this planet
     * @param productionRate rate at which factory outputs new units
     */
    public Node(final Game game, final double x, final double y, final Player owner, final FleetAttributes attributes, final double productionRate)
    {
        this.game = game;
        position = new Position(x, y);
        factory = new Factory(game, this, attributes, productionRate);
        defendingFormation = new Formation(game, owner, 0, new FleetAttributes(0.0, 1.0));
    }
    
    public Position getPosition()
    {
        return position;
    }

    /**
     * @return the owner of defending formation
     */
    public Player getOwner()
    {
        return defendingFormation.getOwner();
    }
    
    /**
     * @return defending formation
     */
    public Formation getDefendingFormation()
    {
        return defendingFormation;
    }

    public Factory getFactory()
    {
        return factory;
    }
    
    /**
     * Transfer half of this defending formation to another node
     * @param node another node
     * @return invoked transfer event
     */
    public Transfer transferUnits(final Node node)
    {
        return new Transfer(game, defendingFormation.extractHalf(), this, node, position.distance(node.position));
    }
    
    /**
     * Confront this formation with arriving formation
     * @param formation arriving formation
     */
    public void receiveUnits(final Formation formation)
    {
        defendingFormation = defendingFormation.confront(formation);
    }
}
