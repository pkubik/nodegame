package pw.eiti.pkubik.proz.game.model;

import javafx.geometry.Point2D;

/**
 * Represents immutable 2D point
 * Wraps JavaFX Point2D
 * Two Positions holding the same coordinates can be considered equal
 */
public class Position
{
    /** Wrapped class */
    private final Point2D point;
    
    /**
     * @param x X-coordinate
     * @param y Y-coordinate
     */
    public Position(final double x, final double y)
    {
        point = new Point2D(x, y);
    }
    
    public double getX()
    {
        return point.getX();
    }
    
    public double getY()
    {
        return point.getY();
    }
    
    /**
     * @param position another Position
     * @return euclidean distance
     */
    public double distance(final Position position)
    {
        return point.distance(position.point);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        return point.equals(((Position) obj).point);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return point.hashCode();
    }
}
