package pw.eiti.pkubik.proz.game.model;

import java.io.IOException;

/**
 * Game instance root
 */
class Game
{
    /** Manages in-game time */
    private final Clock clock = new Clock(0);
    /** Universe reference */
    private final Universe universe;
    /** Null player */
    private final Player nullPlayer = new Player("none");
    /** Neutral player */
    private final Player neutralPlayer = new Player("none");
    /** Active player, that is performing actions */
    private Player activePlayer = nullPlayer;
    
    public Game() throws IOException
    {
        universe = new Universe(this);
    }

    Clock getClock()
    {
        return clock;
    }

    Player getNeutralPlayer()
    {
        return neutralPlayer;
    }
    
    Player getNullPlayer()
    {
        return nullPlayer;
    }
    
    Player getActivePlayer()
    {
        return activePlayer;
    }
    
    void setActivePlayer(final Player player)
    {
        activePlayer = player;
    }

    Universe getUniverse()
    {
        return universe;
    }
    
    /**
     * Advance the game by given time
     * @param delay in-game time elapsed
     * @return game state after an update
     */
    GameState update(final long delay)
    {
        universe.update(delay);
        
        return new GameState(this);
    }
}
