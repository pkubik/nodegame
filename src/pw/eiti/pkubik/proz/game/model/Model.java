package pw.eiti.pkubik.proz.game.model;

import java.io.IOException;

/**
 * MVC Model facade
 */
public class Model
{
    /** Game instance root */
    private final Game game;

    /**
     * Creates Model connected with specified controller
     * @param controller MVC Controller
     * @throws IOException thrown when failed to load game configuration
     */
    public Model() throws IOException
    {
        game = new Game();
    }

    /**
     * Advance the game by given time
     * @param delay in-game time elapsed
     * @return updated game state
     */
    public GameState update(final long delay)
    {
        return game.update(delay);
    }
    
    /**
     * Sends fleet between to nodes
     * @param source Position of the source node
     * @param target Position of the target node
     */
    public void sendFleet(final Position source, final Position target)
    {
        final Node sourceNode = game.getUniverse().getNodes().get(source);
        final Node targetNode = game.getUniverse().getNodes().get(target);
        game.getUniverse().transferUnits(sourceNode, targetNode);
    }
}
