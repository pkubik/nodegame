package pw.eiti.pkubik.proz.game.model;

/**
 * Maintains independent representation of in-game time
 */
class Clock
{
    /** time in nanoseconds */
    private long time;
    
    /**
     * @param time initial time value
     */
    public Clock(final long time)
    {
        this.time = time;
    }
    
    /**
     * Equivalent of System.nanoTime()
     * @return in-game time
     */
    public long nanoTime()
    {
        return time;
    }
    
    /**
     * Increases in-game time value
     * @param value an amount of time to be added to in-game time
     */
    public void delay(final long value)
    {
        time += value;
    }
}
