package pw.eiti.pkubik.proz.game.model;

class FleetAttributes
{
    private final double attack;
    private final double speed;
    
    public FleetAttributes(final double attack, final double speed)
    {
        this.attack = attack;
        this.speed = speed;
    }

    public FleetAttributes add(final FleetAttributes addend,
                               final int currentSize, 
                               final int addendSize)
    {
        final double totalAttack = attack + addend.attack;
        final double averageSpeed = (speed * currentSize + addend.speed * addendSize)
                                     / (currentSize + addendSize);
        return new FleetAttributes(totalAttack, averageSpeed);
    }
    
    public double getAttack()
    {
        return attack;
    }

    public double getSpeed()
    {
        return speed;
    }
}
