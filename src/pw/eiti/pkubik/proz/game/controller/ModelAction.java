package pw.eiti.pkubik.proz.game.controller;

/**
 * Specifies an action to be performed on model
 */
public interface ModelAction
{
    void process();
}
