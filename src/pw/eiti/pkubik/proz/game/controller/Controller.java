package pw.eiti.pkubik.proz.game.controller;

import java.io.IOException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import pw.eiti.pkubik.proz.game.events.VectorSelectionEvent;
import pw.eiti.pkubik.proz.game.events.ViewEventListener;
import pw.eiti.pkubik.proz.game.model.GameState;
import pw.eiti.pkubik.proz.game.model.Model;
import pw.eiti.pkubik.proz.game.model.Position;
import pw.eiti.pkubik.proz.game.view.View;

/**
 * MVC Controller
 */
public class Controller
{
    private Model model;
    private View view;
    private Thread controllerThread;
    private final BlockingDeque<ModelAction> events = new LinkedBlockingDeque<ModelAction>();
    private boolean running = false;
    private long lastFrameUpdateTime;

    /**
     * Main controller loop
     */
    private void loop()
    {
        ModelAction action;
        lastFrameUpdateTime = System.nanoTime();
        running = true;
        while (running)
        {
            try
            {
                action = events.take();
                action.process();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }           
        }
    }
    
    public void setView(final View view)
    {
        this.view = view;
    }
    
    /**
     * Starts
     * @throws IOException thrown when failed to load game settings
     */
    public void start()
    {
        try
        {
            model = new Model();
            controllerThread = new Thread( this::loop );
            controllerThread.start();
            addViewEventListeners();
        } catch (IOException e)
        {
            view.close();
            e.printStackTrace();
        }
    }
    
    private void addViewEventListeners()
    {
        view.addVectorSelectionEventListener(new ViewEventListener<VectorSelectionEvent>()
        {
            @Override
            public void handle(final VectorSelectionEvent event)
            {
                putAction(new TransferAction(event));
            }
        });
    }

    /**
     * Put the action at the end of queue
     * @param action
     */
    private void putAction(final ModelAction action)
    {
        boolean failed = false;
        do
        {
            try
            {
                events.putLast(action);
            }
            catch (InterruptedException e)
            {
                failed = true;
            }
        }
        while (failed == true);
    }
    
    /**
     * Put the action at the beginning of queue
     * @param event
     */
    private void putImportantAction(final ModelAction event)
    {
        boolean failed = false;
        do
        {
            try
            {
                events.putFirst(event);
            }
            catch (InterruptedException e)
            {
                failed = true;
            }
        }
        while (failed == true);
    }
    
    /**
     * Request game state update from the model
     * @param timeMultiplier defines a rate at which game should be accelerated or decelerated
     */
    public void updateFrame(final double timeMultiplier)
    {
        putImportantAction(new FrameUpdateAction(timeMultiplier));
    }
    
    /**
     * Breaks game loop
     */
    private void stop()
    {
        running = false;
    }
    
    /**
     * Performs gracefully exit
     */
    public void exit()
    {
        putImportantAction( this::stop );
    }
    
    /**
     * Propagate game state to the view
     * @param gameState current game state
     */
    public void forwardUpdate(final GameState gameState)
    {
        view.updateView(gameState);
    }
    
    /**
     * Unit transfer action
     */
    class TransferAction implements ModelAction
    {
        private final Position source;
        private final Position target;
        
        public TransferAction(final VectorSelectionEvent event)
        {
            this.source = event.getStart();
            this.target = event.getEnd();
        }
        
        @Override
        public void process()
        {
            model.sendFleet(source, target);
        }
    }
    
    /**
     * Requests model update
     */
    class FrameUpdateAction implements ModelAction
    {   
        private final double timeMultiplier;
        
        public FrameUpdateAction(final double timeMultiplier)
        {
            this.timeMultiplier = timeMultiplier;
        }

        @Override
        public void process()
        {
            final long currentTime = System.nanoTime();
            final long delay = (long) ((currentTime - lastFrameUpdateTime) * timeMultiplier);
            forwardUpdate(model.update(delay));
            lastFrameUpdateTime = currentTime;
        }
    }
}
