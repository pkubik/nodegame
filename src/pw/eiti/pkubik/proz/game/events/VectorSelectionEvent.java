package pw.eiti.pkubik.proz.game.events;

import pw.eiti.pkubik.proz.game.model.Position;

/**
 * Occurs when user selects a pair of entities
 * For example by dragging one onto another
 * or clicking them sequentially
 */
public class VectorSelectionEvent implements ViewEvent
{
    /** Center of the first entity */
    private final Position start;
    /** Center of the second entity */
    private final Position end;

    /**
     * Creates event indicating selection of two entities
     * @param start center of the first entity
     * @param end center of the second entity
     */
    public VectorSelectionEvent(final Position start, final Position end)
    {
        this.start = start;
        this.end = end;
    }
    
    /**
     * @return center of the first entity
     */
    public Position getStart()
    {
        return start;
    }

    /**
     * @return center of the second entity
     */
    public Position getEnd()
    {
        return end;
    }
}
