package pw.eiti.pkubik.proz.game.events;

public interface ViewEventListener<E extends ViewEvent>
{
    void handle(final E event);
}
