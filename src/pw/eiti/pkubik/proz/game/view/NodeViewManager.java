package pw.eiti.pkubik.proz.game.view;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.Pane;
import pw.eiti.pkubik.proz.game.events.VectorSelectionEvent;
import pw.eiti.pkubik.proz.game.model.GameState;
import pw.eiti.pkubik.proz.game.model.NodeState;
import pw.eiti.pkubik.proz.game.model.Position;

class NodeViewManager
{
    private final GUI gui;
    private final Pane pane;
    private Map<Position, NodeView> nodeViews;
    
    private NodeView pointedNode;
    private NodeView clickedNode;
    
    public NodeViewManager(final GUI gui, final Pane pane)
    {
        this.gui = gui;
        this.pane = pane;
        nodeViews = new HashMap<Position, NodeView>();
        
        pane.setOnMouseClicked(new EventHandler<Event>()
        {
            @Override
            public void handle(Event event)
            {
                if (clickedNode != null)
                {
                    clickedNode.unmarkAsClicked();
                    clickedNode = null;
                }
            }
        });
    }
    
    /**
     * Updates graphics entities to match current game state
     * @param gameState current game state
     */
    public void update(final GameState gameState)
    {
        final List<NodeState> nodes = gameState.getNodes();
        Map<Position, NodeView> rejected = new HashMap<Position, NodeView>(nodeViews);
        Set<Position> states = new HashSet<Position>(nodes.size());
        for (NodeState node : nodes)
        {
            states.add(node.getPosition());
            NodeView nodeView = nodeViews.get(node.getPosition());
            if (nodeView == null)
            {
                nodeView = new NodeView(this, node.getPosition());
                pane.getChildren().add(nodeView.getGraphicNode());
                nodeViews.put(node.getPosition(), nodeView);
            }
            final PlayerView owner = gui.getPlayerViewManager().getPlayer(node.getOwnerId());
            nodeView.update(node, owner);
        }
        rejected.keySet().removeAll(states);
        nodeViews.keySet().retainAll(states);
        
        for (NodeView nodeView : rejected.values())
        {
            pane.getChildren().remove(nodeView.getGraphicNode());
        }
        
        if (pointedNode != null)
        {
            gui.setHintText(pointedNode.getPropertiesText());
        }
        else
        {
            gui.setHintText("---");
        }
    }

    public NodeView getPointedNode()
    {
        return pointedNode;
    }

    public void setPointedNode(NodeView pointedNode)
    {
        if (pointedNode != null)
        {
            gui.setHintText(pointedNode.getPropertiesText());
        }
        else
        {
            gui.setHintText("---");
        }
        this.pointedNode = pointedNode;
    }

    public void nodeClicked(final NodeView node)
    {
        if (clickedNode == null)
        {
            clickedNode = node;
        }
        else
        {
            clickedNode.unmarkAsClicked();
            node.unmarkAsClicked();
            gui.notifyAboutEvent(new VectorSelectionEvent(clickedNode.getPosition(), node.getPosition()));
            clickedNode = null;
        }
    }
    
    public void nodeUnclicked(final NodeView node)
    {
        clickedNode = null;
    }
    
    public NodeView get(final Position position)
    {
        return nodeViews.get(position);
    }
}
