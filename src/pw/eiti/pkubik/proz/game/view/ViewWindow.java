package pw.eiti.pkubik.proz.game.view;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pw.eiti.pkubik.proz.game.controller.Controller;

/**
 * Manages external UI features, and global events
 */
class ViewWindow
{
    /** Aimed frames per second value */
    private static double FPS = 60.0;
    /** Delay between each frame update request */
    private static long FRAME_DURATION = (long) (1000/FPS);
    
    private final View view;
    private final Controller controller;
    private GUI gui;
    private final Stage stage;
    private final Timer timer = new Timer();
    
    ViewWindow(final View view, final Stage stage, final Controller controller)
    {
        this.view = view;
        this.stage = stage;
        this.controller = controller;
        initialize();
    }
    
    /**
     * Load GUI using FXML file
     */
    private void initialize()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Circles.fxml"));
            final Scene scene = new Scene(fxmlLoader.load());
            gui = (GUI) fxmlLoader.getController();
            initializeStage(scene);            
            initializeWindowEvents(scene);
            scheduleFrameUpdates();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            stage.close();
        }
    }

    /**
     * @param scene JavaFX scene
     */
    private void initializeStage(final Scene scene)
    {
        stage.setTitle("Eufloria Clone");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Initialize global events
     * @param scene JavaFX scene
     */
    private void initializeWindowEvents(final Scene scene)
    {
        stage.setOnCloseRequest(new EventHandler<WindowEvent>()
        {          
            @Override
            public void handle(WindowEvent event)
            {
                close();
            }
        });
        
        scene.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                if (event.getCode() == KeyCode.SPACE)
                {
                    view.togglePause();
                }
                else if (event.getCode() == KeyCode.ESCAPE)
                {
                    close();
                }
            }
        });
        
        scene.setOnScroll(new EventHandler<ScrollEvent>()
        {
            @Override
            public void handle(ScrollEvent event)
            {
                final double multiplier = view.getTimeMultiplier();
                view.setTimeMultiplier(multiplier + event.getDeltaY() / event.getMultiplierY() * 0.1);
            }
        });
    }
    
    /**
     * Ensure regular model updates
     */
    private void scheduleFrameUpdates()
    {
        timer.scheduleAtFixedRate(new TimerTask()
        {         
            @Override
            public void run()
            {
                controller.updateFrame(view.getTimeMultiplier());
            }
        }, 0, FRAME_DURATION);
    }
    
    /**
     * @return GUI controller specified in FXML file
     */
    GUI getGUI()
    {
        return gui;
    }
    
    /**
     * Close window (closes whole View)
     */
    void close()
    {
        timer.cancel();
        stage.close();
    }

}
