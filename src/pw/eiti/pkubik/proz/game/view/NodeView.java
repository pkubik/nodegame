package pw.eiti.pkubik.proz.game.view;

import pw.eiti.pkubik.proz.game.model.NodeState;
import pw.eiti.pkubik.proz.game.model.Position;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.Bloom;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

class NodeView
{
    private static final Paint DEFAULT_BORDER_PAINT = Paint.valueOf("black");
    
    private final Position position;
    private String propertiesText = "---";
    private IntegerProperty formationSize = new SimpleIntegerProperty(0);
    private final Pane pane;
    private final Shape shape;
    private final Label label;
    private boolean markedAsClicked = false;
    
    public NodeView(final NodeViewManager nodeViewManager, final Position position)
    {
        this.position = position;
        shape = new Circle(30.0);
        shape.setStroke(DEFAULT_BORDER_PAINT);
        shape.setStrokeWidth(3);
        label = new Label();
        label.setTextFill(Paint.valueOf("white"));
        label.textProperty().bind(Bindings.convert(formationSize));
        pane = new StackPane(shape, label);
        pane.setLayoutX(position.getX()*100-30);
        pane.setLayoutY(position.getY()*100-30);
        pane.setOnMouseEntered(new EventHandler<Event>()
        {
            @Override
            public void handle(Event event)
            {
                nodeViewManager.setPointedNode(NodeView.this);
                label.setEffect(new Bloom());
                event.consume();
            }
        });
        pane.setOnMouseExited(new EventHandler<Event>()
        {
            @Override
            public void handle(Event event)
            {
                nodeViewManager.setPointedNode(null);
                label.setEffect(null);
                event.consume();
            }
        });
        pane.setOnMouseClicked(new EventHandler<Event>()
        {
            @Override
            public void handle(Event event)
            {
                if (markedAsClicked)
                {
                    unmarkAsClicked();
                    nodeViewManager.nodeClicked(NodeView.this);
                }
                else
                {
                    markAsClicked();
                    nodeViewManager.nodeClicked(NodeView.this);
                }
                event.consume();
            }
        });
    }
    
    public void update(final NodeState state, final PlayerView owner)
    {
        this.formationSize.set(state.getFormationSize());
        this.propertiesText = "Atk: " + state.getFormationAttack() + " | " +
                              "Spd: " + state.getFormationSpeed() + " | " +
                              "Owner: " + owner.getName() + " | " +
                              "P. Rate: " + state.getProductionRate();
        shape.setFill(owner.getPaint());
    }

    public String getPropertiesText()
    {
        return propertiesText;
    }
    public Node getGraphicNode()
    {
        return pane;
    }

    public Position getPosition()
    {
        return position;
    }
    
    public void markAsClicked()
    {
        markedAsClicked = true;
        shape.setStroke(Paint.valueOf("white"));
    }
    
    public void unmarkAsClicked()
    {
        markedAsClicked = false;
        shape.setStroke(DEFAULT_BORDER_PAINT);
    }
}
