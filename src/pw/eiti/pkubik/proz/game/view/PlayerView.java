package pw.eiti.pkubik.proz.game.view;

import java.util.UUID;

import javafx.scene.paint.Paint;

class PlayerView
{
    
    private final UUID id;
    private final String name;
    private final Paint paint;
    
    public PlayerView(final UUID uuid, final String name, final Paint paint)
    {
        this.id = uuid;
        this.name = name;
        this.paint = paint;
    }

    public UUID getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Paint getPaint()
    {
        return paint;
    }
}
