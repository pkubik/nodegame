package pw.eiti.pkubik.proz.game.view;
import javafx.application.Platform;
import javafx.stage.Stage;
import pw.eiti.pkubik.proz.game.controller.Controller;
import pw.eiti.pkubik.proz.game.events.VectorSelectionEvent;
import pw.eiti.pkubik.proz.game.events.ViewEventListener;
import pw.eiti.pkubik.proz.game.model.GameState;

/**
 * MVC View facade
 */
public class View
{   
    private final Controller controller;
    private final ViewWindow window;
    private final GUI gui;
    private double timeMultiplier = 1;
    private boolean paused = false;
    
    public View(final Stage stage, final Controller controller)
    {
        this.window = new ViewWindow(this, stage, controller);
        this.gui = window.getGUI();
        this.controller = controller;
    }    
	
	public void updateView(final GameState gameState)
	{
	    Platform.runLater( () -> gui.update(gameState) );
	}
	
    /**
     * Sets {@link #VectorSelectionEvent} listener
     * @param listener event listener
     */
	public void addVectorSelectionEventListener(final ViewEventListener<VectorSelectionEvent> listener)
	{
	    gui.addVectorSelectionEventListener(listener);
	}
	
    /**
     * Removes {@link #VectorSelectionEvent} listener
     * @param listener event listener
     */
	public void removeVectorSelectionEventListener(final ViewEventListener<VectorSelectionEvent> listener)
    {
        gui.addVectorSelectionEventListener(listener);
    }
	
	/**
	 * Close application 
	 */
	public void close()
	{
	    window.close();
	}
	
    void togglePause()
    {
        paused = !paused;
    }
    
    boolean isPaused()
    {
        return paused;
    }
    
    double getTimeMultiplier()
    {
        if (isPaused())
            return 0;
        else
            return timeMultiplier;
    }

    void setTimeMultiplier(double timeMultiplier)
    {
        if (timeMultiplier < 0)
            timeMultiplier = 0;
        else if (timeMultiplier > 10)
            timeMultiplier = 10;
        this.timeMultiplier = timeMultiplier;
    }
}
