package pw.eiti.pkubik.proz.game.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.UUID;

import pw.eiti.pkubik.proz.game.model.GameState;
import pw.eiti.pkubik.proz.game.model.PlayerState;
import javafx.scene.paint.Paint;

class PlayerViewManager
{
    private final Stack<Paint> paints;
    private final Paint neutralPaint = Paint.valueOf("grey");
    private final Map<UUID, PlayerView> playerViews;
    
    public PlayerViewManager()
    {
        paints = new Stack<Paint>();
        paints.push(Paint.valueOf("yellow"));
        paints.push(Paint.valueOf("blue"));
        paints.push(Paint.valueOf("darkred"));
        paints.push(Paint.valueOf("green"));
        /* add new color if more players */
        
        playerViews = new HashMap<UUID, PlayerView>();
    };
    
    /**
     * Updates graphics entities to match current game state
     * @param gameState current game state
     */
    public void update(final GameState gameState)
    {
        {
            final PlayerState state = gameState.getNullPlayer();
            PlayerView view = playerViews.get(state.getId());
            if (view == null)
            {
                view = new PlayerView(state.getId(), state.getName(), neutralPaint);
                playerViews.put(state.getId(), view);
            }
        }
        
        for (PlayerState state : gameState.getPlayers())
        {
            PlayerView view = playerViews.get(state.getId());
            if (view == null)
            {
                view = new PlayerView(state.getId(), state.getName(), paints.pop());
                playerViews.put(state.getId(), view);
            }        
        }
    }
    
    public PlayerView getPlayer(final UUID uuid)
    {
        return playerViews.get(uuid);
    }
}
