package pw.eiti.pkubik.proz.game.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javafx.scene.layout.Pane;
import pw.eiti.pkubik.proz.game.model.GameState;
import pw.eiti.pkubik.proz.game.model.TransferState;

class TransferViewManager
{
    private final List<TransferView> transfers = new ArrayList<TransferView>();
    private final NodeViewManager nodeViewManager;
    private final Pane pane;
    
    public TransferViewManager(final NodeViewManager nodeViewManager, final Pane pane)
    {
        this.nodeViewManager = nodeViewManager;
        this.pane = pane;
    }
    
    /**
     * Updates graphics entities to match current game state
     * @param gameState current game state
     */
    public void update(final GameState gameState)
    {
        final List<TransferState> states = gameState.getTransfers();
        final long time = gameState.getTime();
        
        Map<UUID, TransferState> map = new HashMap<UUID, TransferState>();
        for (TransferState state : states)
        {
            map.put(state.getId(), state);
        }
        
        Iterator<TransferView> iterator = transfers.iterator();
        while (iterator.hasNext())
        {
            final TransferView view = iterator.next();
            final TransferState state = map.get(view.getId());
            if (state != null)
            {
                view.update(state, time);
                map.remove(view.getId());
            }
            else
            {
                pane.getChildren().remove(view.getGraphicNode());
                iterator.remove();
            }
        }
        
        for (TransferState state : map.values())
        {
            TransferView view = new TransferView(state, nodeViewManager); 
            transfers.add(view);
            pane.getChildren().add(view.getGraphicNode());
        }
    }
}
