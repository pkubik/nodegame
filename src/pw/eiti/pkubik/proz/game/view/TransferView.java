package pw.eiti.pkubik.proz.game.view;

import java.util.UUID;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import pw.eiti.pkubik.proz.game.model.TransferState;

class TransferView
{
    private final UUID id;
    private final Pane graphicNode;
    private final Line mainLine;
    private final Line progressLine;
    /* direction and length of line */
    private final Point2D vector;
    
    public TransferView(final TransferState state, final NodeViewManager nodeManager)
    {
        id = state.getId();
        mainLine = new Line(state.getSource().getX()*100,
                            state.getSource().getY()*100,
                            state.getTarget().getX()*100,
                            state.getTarget().getY()*100);
        mainLine.setStroke(Paint.valueOf("grey"));
        mainLine.setStrokeWidth(8.0);
        progressLine = new Line(state.getSource().getX()*100,
                                state.getSource().getY()*100,
                                state.getSource().getX()*100,
                                state.getSource().getY()*100);
        progressLine.setStroke(Paint.valueOf("white"));
        progressLine.setStrokeWidth(6.0);
        graphicNode = new Pane(mainLine, progressLine);
        vector = new Point2D(mainLine.getEndX()-mainLine.getStartX(),
                             mainLine.getEndY()-mainLine.getStartY());
        mainLine.setOpacity(0.3);
        graphicNode.setOpacity(0.4);
    }
    
    public UUID getId()
    {
        return id;
    }
    
    public void update(final TransferState state, final long time)
    {
        final double progress = state.getProgress(time);
        final Point2D progressVector = vector.multiply(progress);
        progressLine.setEndX(progressLine.getStartX() + progressVector.getX());
        progressLine.setEndY(progressLine.getStartY() + progressVector.getY());
    }
    
    public Node getGraphicNode()
    {
        return graphicNode;
    }
}
