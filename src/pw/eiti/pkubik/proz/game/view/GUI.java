package pw.eiti.pkubik.proz.game.view;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import pw.eiti.pkubik.proz.game.events.VectorSelectionEvent;
import pw.eiti.pkubik.proz.game.events.ViewEventListener;
import pw.eiti.pkubik.proz.game.model.GameState;


/**
 * Manages graphical user interface
 */
public class GUI implements Initializable
{
    /** StringProperty bound to the hintLabel */
    private final StringProperty hintText = new SimpleStringProperty();
    /** Label on the bottom of the view */
    @FXML private Label hintLabel;
    /** Game viewport */
    @FXML private Pane gameViewPane;
    /** Layer designated to display nodes */
    private final Pane nodesViewPane = new Pane();
    /** Layer designated to display current transfers */
    private final Pane transfersViewPane = new Pane();
    
    /** Manages nodes status display, and nodes events */
    private NodeViewManager nodeViewManager;
    /** Manages transfers display */
    private TransferViewManager transferViewManager;
    /** Manages players distinctions. Supplementary to other managers */
    private PlayerViewManager playerViewManager;
    
    /** Listeners of VectorSelectionEvent */
    private List<ViewEventListener<VectorSelectionEvent>> vectorSelectionEventListeners = 
            new LinkedList<ViewEventListener<VectorSelectionEvent>>();
    
    /**
     * Called while loading FXML file, initializes main stage nodes
     * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
     */
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        hintLabel.textProperty().bind(hintText);
        gameViewPane.getChildren().add(transfersViewPane);
        gameViewPane.getChildren().add(nodesViewPane);
        playerViewManager = new PlayerViewManager();
        nodeViewManager = new NodeViewManager(this, nodesViewPane);
        transferViewManager = new TransferViewManager(nodeViewManager, transfersViewPane);
    }
    
    /**
     * Sets bottom label text
     * @param text text to be set
     */
    void setHintText(final String text)
    {
        hintText.set(text);
    }
    
    /**
     * Updates the GUI according to supplied state
     * @param gameState current game state
     */
    public void update(final GameState gameState)
    {
        playerViewManager.update(gameState);
        nodeViewManager.update(gameState);
        transferViewManager.update(gameState);
    }
    
    PlayerViewManager getPlayerViewManager()
    {
        return playerViewManager;
    }

    /**
     * Sets {@link #VectorSelectionEvent} listener
     * @param listener event listener
     */
    public void addVectorSelectionEventListener(ViewEventListener<VectorSelectionEvent> listener)
    {
        vectorSelectionEventListeners.add(listener);
    }
    
    /**
     * Removes {@link #VectorSelectionEvent} listener
     * @param listener event listener
     */
    public void removeVectorSelectionEventListener(ViewEventListener<VectorSelectionEvent> listener)
    {
        vectorSelectionEventListeners.add(listener);
    }

    /**
     * Notify all listeners about {@link #VectorSelectionEvent}
     * @param vectorSelectionEvent
     */
    public void notifyAboutEvent(final VectorSelectionEvent vectorSelectionEvent)
    {
        for (ViewEventListener<VectorSelectionEvent> listener : vectorSelectionEventListeners)
        {
            listener.handle(vectorSelectionEvent);
        }
    }
}
